#ifndef MKR_SERIALIZATION_H_INCLUDED
#define MKR_SERIALIZATION_H_INCLUDED

#include <stdio.h>
#include <vector>

//methods
template <typename To>
bool serialize(To* o, char * _fileName){
    FILE *f = fopen(_fileName, "wb");

    int len(0);

    if(f){
        len = fwrite(o, sizeof(*o), 1, f);
    }

    fclose(f);

    return (len == 1);
}

template <typename To>
To deserialize(char * _fileName){
    FILE *f = fopen(_fileName, "rb");
    To o;

    if(f){
        fread(&o, sizeof(To), 1, f);
    }

    fclose(f);

    return (o);
}

template <typename To>
bool serialize(std::vector<To>* vo, char* _filename){
    FILE *f = fopen(_filename, "wb");
    long len(0);

    if(f){
        for(size_t i = 0; i < vo->size(); i++){
            len += fwrite(&(vo->at(i)), sizeof(To), 1, f);

        }

    }

    fclose(f);

    return (len == (vo->size()));
}

template <typename To>
std::vector<To> deserialize_vector(char* _filename){
    FILE *f = fopen(_filename, "rb");
    std::vector<To> vo;

    if(f){
        To o;
        int length;

        while((length = fread(&o, sizeof(To), 1, f)) > 0){
            vo.push_back(o);
        }
    }

    fclose(f);

    return vo;
}

#endif // MKR_SERIALIZATION_H_INCLUDED


/* Dr Mokira le 22/02/2018 */
