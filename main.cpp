#include <iostream>
#include <string>
#include <vector>

#include <stdlib.h>

#include "mkr_serialization.h"

using namespace std;


class Object{
    private :
        string name = "";
        unsigned int color = 0x00;
        int size_ = 0;
        //float count_[10];
    public :
        Object(){

        }
        Object(string str, unsigned int c, int s /*float co[10]*/): name(str),
                color(c), size_(s)
        {
            /*for(int i = 0; i < 100; i++)
                count_[i] = co[i];*/
        }

        void display_datas() const{
            cout<<"name = "<<name<<", color = "<<color<<", size = "<<size_<<endl;
            /*for(int i = 0; i < 100; i++)
                cout<<count_[i]<<" ";*/
        }
};


int main()
{
    system("MODE CON COLS=1200 LINES=500");
    Object o0("House one", 0xff000000, 200);
    Object o1("House two", 0xff650000, 300);
    Object o2("House three", 0x0000ff00, 800);

    vector<Object> ol;
    ol.push_back(o0);
    ol.push_back(o1);
    ol.push_back(o2);

    for(size_t i = 0; i < ol.size(); i++)
        ol[i].display_datas();

    cout<<"Serialization .. "<<endl;
    serialize<Object>(&ol, (char*)"object.bin");
    cout<<"done !\n"<<endl;

    cout<<"Deserialization .. "<<endl;
    vector<Object> ol1 = deserialize_vector<Object>((char*)"object.bin");
    cout<<"done !\n"<<endl;
    for(size_t i = 0; i < ol1.size(); i++)
        ol1[i].display_datas();

    system("PAUSE");

    return 0;
}
